# TransformInterpolate plugin for Nuke
Created by Hendrik Proosa.
Licence is: do whatever you want with it.

Binary files are currently compiled for:
- Linux (tested on Centos 7): Nuke 11.3, 12.0, 12.1, 12.2
- Win: Nuke 11.3, 12.0, 12.1, 12.2


## Build

### Linux (Centos)

Modify build_linux.sh file so that it points to appropriate directories and run it from shell.

### Windows

Modify CMakeLists.txt file so that it points to appropriate directories and build using appropriate compiler for Win Nuke versions. For Nuke 11 and above it is MSVC v19.00, manifest 19.00.24210 or similar.

## Install

Copy the hpTools folder to Nuke plugin dir and add lines from init.py and menu.py to respective files. Script loads proper binary plugin for launched Nuke version.


## What it does

Node takes two inputs, B and A and linearly interpolates between their transforms based on dissolve factor value. If inputs are not derived from Transform op, their incoming matrix will be identity matrix, meaning has no effect. This property can be used for dissolving between transformed and original image by simply leaving A input disconnected.

## How to use

Connect image input to B, this is where the transformed image data comes from, and optionally connect some other transform to input A. Input A does not have to be connected to any image, because transform does not rely on having any image data.

Dissolve uses 4x4 transform matrices internally, which means that it does not interpolate the translation, rotation, scale etc values as seen in Transform node. This has both its upsides and also downs. Upside is that it can easily interpolate between a cornerpin (perspective transform) and basic transform (affine, with no perspective component). Downside is that there is no right way to interpolate matrices so that it produces a visually meaningful result. Decomposition to original transform components is nontrivial and might not produce weird values. Thus, interpolating over these decomposed components is also problematic in some cases. 

Regarding motion blur, there is a small problem with UI freesing for a few seconds that shows up in some cases, usually when switching between nodes and when properties panel is open. Not sure what the reason is, I suspect something related to context switching which is necessary for proper motion blur. Does not happen when motion blur is not enabled, so workaround is to enable motion blur only for rendering.

## Changelog

### 12. november 2020
Nuke 12.2 versions added.

### 05. september 2020
Improved motion blur handling and new binaries.

### 21. april 2020
Changed name to TransformInterpolate due to name chash with a gizmo in Nukepedia

### 20. april 2020
Initial version that works (motion blur, proxy mode etc) but interpolation method itself could be improved.
